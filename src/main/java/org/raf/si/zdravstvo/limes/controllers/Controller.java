package org.raf.si.zdravstvo.limes.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/limes")
public class Controller {

    @GetMapping("/hello")
    public String hello() {
        return "LiMeS says hello";
    }

}
