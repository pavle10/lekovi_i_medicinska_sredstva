package org.raf.si.zdravstvo.limes.camel;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class TestRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("timer:foo?period=3s").id("CamelTestRoute")
                .process(exchange -> {
                    exchange.getOut().setBody("ProcessorTest");
                })
                .log("LiMeS meets Camel! Message body: ${body}");
    }
}
